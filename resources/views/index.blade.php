@extends('layout.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add Product</div>
                <div class="panel-body">
                    <form role="form" method="GET" id="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <label for="product_name">Product Name</label>
                                            <input type="text" class="form-control" id="product_name" placeholder="Enter Product name" name="product_name">
                                        </div>   
                                        <div class="form-group form-md-line-input">
                                            <label for="quantity">Quantity</label>
                                            <input type="number" min="0" value="0" class="form-control" id="quantity" placeholder="Enter Quantity" name="quantity">
                                        </div>   
                                        <div class="form-group form-md-line-input">
                                            <label for="price">Price Per Item</label>
                                            <input type="number" min="0" value="0" class="form-control" id="price" placeholder="Enter Price" name="price">
                                        </div>   
                                        <div class="form-group form-md-line-input">
                                         <button type="button" class="btn green" onclick="submitData('product-add')" id="static">Add</button>
                                     </div>                      
                                 </div>

                             </div>
                         </div>
                     </div>
                 </form>
             </div>


             <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th> Product Name </th>
                            <th> Quantity in stock </th>
                            <th> Price per item </th>
                            <th> Datetime submitted </th>
                            <th>Total value number</th>
                        </tr>
                    </thead>
                    <tbody id="datavalue">
                        <tr>
                            <th colspan="4">Total</th>
                            <td id="total"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>
</div>
</div>
@endsection
