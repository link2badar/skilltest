<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Skilltest</title>


	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

	<!-- Styles -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script type="text/javascript">
		//global variables
	var site_url = "{{ url('/') }}";
	var totalvalue = 0;
	var grandtotal = 0;
		function submitData(url) {
        $("#static").html('<p style="text-align: center;"><i class="fa fa-spinner fa-spin"></i>  Please wait loading...</p>');
        url = site_url +'/'+ url;
        $.ajax({
            type: "GET",
            cache: false,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : $('#form').serialize(),
            url: url,
            success: function(result) {
            	price = parseInt(result.price);
            	quantity = parseInt(result.quantity);

            	totalvalue = price*quantity;
            	grandtotal = grandtotal + totalvalue;
            	
            	var datavalue = '<tr><td>'+result.product_name+'</td><td>'+result.quantity+'</td><td>'+price+'</td><td>'+result.datetime+'</td><td>'+ totalvalue +'</td></tr>';
                $("#datavalue").prepend(datavalue);
                $('#form')[0].reset();
                $("#static").html("Add");
                $("#total").html(grandtotal);
            }
        });
    }
	</script>

</head>
<body>
	<div id="app">
		<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
			<div class="container">
				<a class="navbar-brand" href="{{ url('/') }}">
					SkillTest
				</a>
			</div>
		</nav>
		<main class="py-4">
			@yield('content')
		</main>
	</div>

</body>
</html>
