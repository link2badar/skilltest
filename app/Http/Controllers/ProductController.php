<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function add(Request $request) {

            $data = $request->all();
            $data['datetime'] = date("d/m/Y h:i:s A");
            unset($data['_token']);
            return json_encode($data);
    }
}
